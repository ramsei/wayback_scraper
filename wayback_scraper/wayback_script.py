import requests
import json
import pymysql
from datetime import datetime
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
#database configuration settings

username = config['database']['Username']
password = config['database']['Password']
DB_Name = config['database']['DB_Name']
DB_HOST = config['database']['DB_HOST']
base_url = config['url']['base_url']
archive_url = config['url']['archive_url']
url_type = config['url']['type']
sitename = config['url']['sitename']

db = pymysql.connect(DB_HOST, username, password, DB_Name)
cursor = db.cursor()


r = requests.get(f"http://web.archive.org/cdx/search/cdx?url={archive_url}&fl=timestamp&output=json")
timestamp = json.loads(r.content)

def get_wayback_urls():
    for url in range(1, len(timestamp)):
        sql_insert = "INSERT INTO `seed_url`(`url`, `wayback_date`, `sitename`, `type`)VALUES(%s, %s, %s, %s)"
        date = str(timestamp[url][0])
        year = date[:4]
        month = date[4:6]
        day = date[6:8]
        wayback_url = f"{base_url}{timestamp[url][0]}/{archive_url}"
        wayback_date = datetime.strptime(f"{year}-{month}-{day}", "%Y-%m-%d").strftime("%Y-%m-%d")
        data = (wayback_url, wayback_date, sitename, url_type)
       # print(data)
        try:
            cursor.execute(sql_insert, data)
            db.commit()

        except:
            continue



get_wayback_urls()
